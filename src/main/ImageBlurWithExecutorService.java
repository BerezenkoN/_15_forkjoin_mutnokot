package main;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;

/**
 * Created by heinr on 13.11.2016.
 */
public class ImageBlurWithExecutorService {
    private static int imgSize;

    private static int mBlurWidth = 13;

    public static void main(String[] args) {
        try {
            System.out.println("Loading image...");

            BufferedImage loadImg = ImageIO.read(new File("cat.jpg"));
            System.out.println("Image loaded.");

            imgSize = loadImg.getWidth();

            System.out.println("Processing image...");
            BufferedImage rImg = process(loadImg);
            System.out.println("Image processed");

            System.out.println("Saving image...");
            ImageIO.write(rImg, "jpg", new File("out.jpg"));
            System.out.println("Image saved");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public static int getmBlurWidth() {
        return mBlurWidth;
    }

    private static BufferedImage process(BufferedImage lImg) throws ExecutionException, InterruptedException {

        int[] rgb = imgToRgb(lImg);

        long start = System.currentTimeMillis();

        int[] transformed = blurParallel(rgb, 2, 100);
        long end = System.currentTimeMillis();

        System.out.println((end - start) + "ms");

               return rgbToImg(transformed);
    }

    private static int[] blurParallel(int[] rgb, int threadsCount, int tasksCount) throws ExecutionException, InterruptedException {

        int[] res = new int[rgb.length];

        // преобразую линейный массив в двумерный
        int vertical = rgb.length / imgSize;
        int[][] square = new int[imgSize][vertical];

        for (int i = 0; i < imgSize; i++) {
            for (int j = 0; j < vertical; j++) {
                square[i][j] = rgb[i * imgSize + j];
            }
        }

        ForkAction forkAction = new ForkAction(0, imgSize);
        forkAction.setSquare(square,tasksCount);
        forkAction.invoke();

        ForkJoinPool forkJoinPool = new ForkJoinPool(threadsCount);
        Void invoke = forkJoinPool.invoke(forkAction);

        for (int i = 0; i < imgSize; i++) {
            for (int j = 0; j < vertical; j++) {
                res[i * imgSize + j] = forkAction.getResult()[i][j];
            }
        }

        return res;
    }

    private static int[] imgToRgb(BufferedImage img) {
        int[] res = new int[imgSize * imgSize];
        img.getRGB(0, 0, imgSize, imgSize, res, 0, imgSize);
        return res;
    }

    private static BufferedImage rgbToImg(int[] rgb) {
        BufferedImage res = new BufferedImage(imgSize, imgSize, BufferedImage.TYPE_INT_RGB);
        res.setRGB(0, 0, imgSize, imgSize, rgb, 0, imgSize);
        return res;
    }
}
